import React, { Component } from "react";
import Form from "./components/Form";
import "./App.css";

class App extends Component {
    render() {
        return (
            <div className="p-3 d-flex flex-column min-vh-100 justify-content-center">
                <Form />
            </div>
        );
    }
}

export default App;
