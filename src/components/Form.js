import React, { Component } from "react";
import validator from 'validator';
import Success from "./Success";

class Form extends Component {
    constructor(props) {
        super(props);

        this.FORM_STATE = {
            notComplete: "not complete",
            completed: "completed",
        };

        this.state = {
            firstName: "",
            lastName: "",
            age: "",
            role: "",
            email: "",
            password: "",
            cnfmPassword: "",
            tos: false,
            errors: {},
            status: this.FORM_STATE.notComplete,
        };
    }

    handleInput = (event) => {
        let {name, value} = event.target;
        let {errors} = this.state;
        delete errors[name];

        if(name === "tos"){
            value = event.target.checked;
        }

        this.setState({
            [name] : value,
            errors,
        })
    }

    handleSubmit = (event) => {
        event.preventDefault();

        const {
            firstName,
            lastName,
            age,
            role,
            gender,
            email,
            password,
            cnfmPassword,
            tos,
        } = this.state;

        const errorsObj = {}

        if(firstName.trim().length === 0){
            errorsObj.firstName = "First Name cannot be empty";
        }
        else if(!validator.isAlpha(firstName.trim())){
            errorsObj.firstName = "Name should only contain letters";
        }

        if(lastName.trim().length === 0){
            errorsObj.lastName = "Last Name cannot be empty";
        }
        else if(!validator.isAlpha(lastName.trim())){
            errorsObj.lastName = "Name should only contain letters";
        }

        if(age.trim().length === 0){
            errorsObj.age = "Age cannot be empty";
        }
        else if(!validator.isInt(age.trim())){
            errorsObj.age = "Age should be a number";
        }
        else if(age <= 0){
            errorsObj.age = "Age cannot be less than 0";
        }

        if(!role){
            errorsObj.role = "Please select your role";
        }

        if(!gender){
            errorsObj.gender = "Please select your gender";
        }

        if(!validator.isEmail(email)){
            errorsObj.email = "Please provide valid email";
        }

        if(!validator.isStrongPassword(password)){
            errorsObj.password = "Password must contain atleast: 1 lowercase, 1 uppercase, 1 number, 1 symbol and should be 8 characters long"
        }

        if(password !== cnfmPassword){
            errorsObj.cnfmPassword = "Password does not match";
        }

        if(!tos){
            errorsObj.tos = "Please accept terms and conditions";
        }

        if(Object.keys(errorsObj).length === 0){
            this.setState({
                status: this.FORM_STATE.completed
            })
        } else {
            this.setState({
                errors: errorsObj,
                status: this.FORM_STATE.notComplete
            })
        }
    }

    render() {

        const {
                firstName,
                lastName,
                age,
                email,
                password,
                cnfmPassword,
                tos,
                errors
            } = this.state;

        return (
            <>
                {this.state.status === this.FORM_STATE.notComplete &&
                <form onSubmit={this.handleSubmit} className="d-flex flex-column rounded-4 p-4 bg-light shadow-lg align-self-center position-relative">
                    <legend className="text-center">SIGN UP</legend>
                    <div className="row pb-3">
                        <div className="form-group col">
                            <label htmlFor="firstName"><i className="fa-solid fa-user"></i> First Name</label>
                            <input 
                                type="text" 
                                className="form-control text-capitalize" 
                                id="firstName" 
                                name= "firstName"
                                placeholder="Enter First Name" 
                                value={firstName}
                                onChange={this.handleInput}
                            />
                            <p className="text-danger ">{errors.firstName}</p>
                        </div>

                        <div className="form-group col">
                            <label htmlFor="lastName"><i className="fa-solid fa-user"></i> Last Name</label>
                            <input 
                                type="text" 
                                className="form-control text-capitalize" 
                                id="lastName" 
                                name="lastName"
                                placeholder="Enter Last Name"
                                value={lastName}
                                onChange={this.handleInput}
                            />
                            <p className="text-danger ">{errors.lastName}</p>
                        </div>
                    </div>

                    <div className="row pb-3">
                        <div className="form-group col">
                            <label htmlFor="age"><i className="fa-solid fa-person"></i> Age</label>
                            <input 
                                type="text" 
                                className="form-control" 
                                id="age" 
                                name="age"
                                placeholder="Enter Age"
                                value={age}
                                onChange={this.handleInput}
                            />
                            <p className="text-danger">{errors.age}</p>
                        </div>

                        <div className="form-group col">
                            <label htmlFor="role"><i className="fa-solid fa-user-tie"></i> Role</label>
                            <select className="form-select" id="role" name="role" aria-label="Default select example" onChange={this.handleInput}>
                                <option value="">Select your role</option>
                                <option value="developer">Developer</option>
                                <option value="seniorDeveloper">Senior Developer</option>
                                <option value="leadDeveloper">Lead Developer</option>
                                <option value="CTO">CTO</option>
                            </select>
                            <p className="text-danger ">{errors.role}</p>
                        </div>
                    </div>
                    
                    <div className="mb-3 pb-3">
                        <label className="form-label"><i className="fa-solid fa-venus-mars"></i> Gender</label>
                        <br className="invisible"></br>
                        <div className="form-check form-check-inline">
                            <label className="form-check-label" htmlFor="male">Male</label>
                            <input className="form-check-input" type="radio" name="gender" value="male" id="male" onChange={this.handleInput}/>
                        </div>
                        <div className="form-check form-check-inline">
                            <label className="form-check-label" htmlFor="female">Female</label>
                            <input className="form-check-input" type="radio" name="gender" value="female" id="female" onChange={this.handleInput}/>
                        </div>
                        <div className="form-check form-check-inline">
                            <label className="form-check-label" htmlFor="others">Others</label>
                            <input className="form-check-input" type="radio" name="gender" value="others" id="others" onChange={this.handleInput}/>
                        </div>
                        <p className="text-danger ">{errors.gender}</p>
                    </div>

                    <div className="form-group pb-3">
                        <label htmlFor="email"><i className="fa-solid fa-envelope"></i> Email</label>
                        <input 
                            type="text" 
                            className="form-control" 
                            id="email" 
                            name="email"
                            aria-describedby="emailHelp" 
                            placeholder="Enter Email" 
                            value={email}
                            onChange={this.handleInput}
                        />
                        <p className="text-danger ">{errors.email}</p>
                    </div>

                    <div className="form-group pb-3">
                        <label htmlFor="password"><i className="fa-sharp fa-solid fa-key"></i> Password</label>
                        <input 
                            type="password" 
                            className="form-control" 
                            id="password" 
                            name= "password"
                            placeholder="Password" 
                            value={password}
                            onChange={this.handleInput}
                        />
                        <p className="text-danger ">{errors.password}</p>
                    </div>

                    <div className="form-group pb-3">
                        <label htmlFor="cnfmPassword"><i className="fa-sharp fa-solid fa-key"></i> Confirm Password</label>
                        <input 
                            type="password" 
                            className="form-control" 
                            id="cnfmPassword" 
                            name="cnfmPassword"
                            placeholder="Confim Password"
                            value={cnfmPassword}
                            onChange={this.handleInput}
                        />
                        <p className="text-danger ">{errors.cnfmPassword}</p>
                    </div>

                    <div className="form-group pb-3">
                        <input 
                            type="checkbox" 
                            className="form-check-input" 
                            id="tos" 
                            name="tos"
                            checked={tos}
                            onChange={this.handleInput}
                        />
                        <label className="form-check-label ms-2" htmlFor="tos">Accept Terms and Conditions</label>
                        <p className="text-danger ">{errors.tos}</p>
                    </div>
                    <button type="submit" className="btn btn-dark">Submit</button>
                </form>
                }

                {
                    this.state.status === this.FORM_STATE.completed && <Success />
                }
            </>
        );
    }
}

export default Form;