import React, { Component } from 'react'

class Success extends Component {
  render() {
    return (
      <div className='d-flex flex-column align-items-center rounded-5 px-4 py-5 bg-light shadow-lg align-self-center'>
        <i class="fa-solid fa-circle-check py-4"></i>
        <span>Thankyou for signing up !</span>
      </div>
    )
  }
}

export default Success;